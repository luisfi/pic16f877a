El programa suma o resta dos números de 3 bits.

	Leera los datos del puerto C, que es de 8 bits, donde:
	Bits	Dato
	0-2	Operando A
	3-5	Operando B
	6	Operación
	7	No tiene uso
	La interpretación del bit de operación es:
	0 --> Resta
	1 --> Suma

El resultado se refleja en el puerto B, el cual esta códificado para un display
de 7 Segmentos de Cátodo Cómun.

Sólo se mostrará hasta el número 9. Del 10 al
14 se mostrará "-".

 Para representar el signo negativo se utiliza el punto.
