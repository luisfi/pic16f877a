processor 16F877A
	include <p16f877a.inc>

OPA equ 0x20 ;Dirección operando A
OPB equ 0x21 ;Dirección operando B
OPR equ 0x22 ;Dirección determinar operador
RESUL equ 0x23 ;Dirección resultado , sólo cuando es < 0
IN equ 0xff ;PORTC entradas
MASKA equ 0x07 ;Mascara bits 0,2
MASKB equ 0x38 ;Mascara bits 3,5
MASKR equ 0x40 ;Mascara bit 6

	org 0
		goto inicio
	org 5
;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Configuración de puertos ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;
inicio:	bsf STATUS,5 ;Banco 1
		clrf TRISB ;PORTB salidas
		movlw IN
		movwf TRISC ;PORTC entradas
		bcf STATUS,5 ;Banco 0
;;;;;;;;;;;;;;;;;;
; Loop principal ;
;;;;;;;;;;;;;;;;;;
operacion:	movlw MASKA
		movwf OPA
		movlw MASKB
		movwf OPB
		movlw MASKR
		movwf OPR
		movf PORTC, 0 ;Lee entradas
		andwf OPA,1 ;Aplica mascaras
		andwf OPB,1
		andwf OPR,1
		bcf STATUS,0 ;CarryFlag <- 0; para no afectar rrf
		rrf OPB,1 ;Corrimiento(1) de 3 bits operando del 3-5
		rrf OPB,1
		rrf OPB,1
		btfsc OPR,6 ;Si 0 --> a-b
		goto suma ;Si 1 --> a+b
		movf OPB,0 ;Resta
		subwf OPA, 0
		btfsc STATUS, 0 ;Si a-b < 0
		goto limpiar ;Si a-b > 0
		movwf RESUL ;Almacena resultado
		comf RESUL, 0
		addlw 1 ;Complemento a 2
		goto cargar
suma:	movf OPA,0
		addwf OPB,0
limpiar:bcf RESUL, 7 ;Limpia bit de signo, suma >= 0
cargar:call table
		btfss RESUL, 7 ;Si es negativo enciende punto como signo
		goto mostrar
		movwf RESUL
		bsf RESUL, 7
		movf RESUL, 0
mostrar:movwf PORTB ;Carga dígito
		goto operacion
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Devuelve Símbolo para display de 7 Sementos Cátodo Común ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
table:	addwf PCL, 1
		;Se usa "-" si es mayor a 9
		retlw 0x3F ;0
		retlw 0x06 ;1
		retlw 0x5B ;2
		retlw 0x4F ;3
		retlw 0x66 ;4
		retlw 0x6D ;5
		retlw 0x7D ;6
		retlw 0x07 ;7
		retlw 0x7F ;8
		retlw 0x67 ;9
		retlw 0x77 ;A
		retlw 0x7C ;b
		retlw 0x39 ;C
		retlw 0x5E ;d
		retlw 0x79 ;E
		end
